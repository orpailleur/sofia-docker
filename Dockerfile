FROM ubuntu:16.04

ENV BOOST_VERSION 58

RUN apt-get update -qqy && \
    apt-get install -qqy  build-essential git vim premake4 libboost1.${BOOST_VERSION}-all-dev cbp2make strace ltrace

RUN mkdir /sofia
WORKDIR /sofia

RUN git clone https://github.com/AlekseyBuzmakov/LibGastonForSofia
RUN git clone https://github.com/AlekseyBuzmakov/LibgSpanForSofia
RUN git clone https://github.com/AlekseyBuzmakov/FCAPS

WORKDIR /sofia/LibGastonForSofia

RUN git rev-parse HEAD > /GASTON-VERSION

RUN make

WORKDIR /sofia/LibgSpanForSofia

RUN git rev-parse HEAD > /GSPAN-VERSION

RUN premake4 gmake

WORKDIR /sofia/LibgSpanForSofia/build

RUN make

WORKDIR /sofia/FCAPS

RUN git rev-parse HEAD > /SOFIA-VERSION

RUN ./update-makefiles.bash

RUN premake4 gmake

WORKDIR /sofia/FCAPS/build
RUN  sed -i.bak -e '/[\s]*CFLAGS[\s]*\+/s/$/ -fPIC/' SharedModulesLib.make && \
     sed -i.bak -e '/[\s]*CFLAGS[\s]*\+/s/$/ -fPIC/' SharedTools.make && \
     sed -i.bak -e '/[\s]*CFLAGS[\s]*\+/s/$/ -fPIC/' Storages.make && \
     sed -i.bak -e '/[\s]*LDFLAGS[\s]*\+[^\)]/s/$/ -Wl,--no-as-needed/' Sofia-PS.make

RUN make

WORKDIR /sofia/FCAPS/build/release

ENTRYPOINT ["/sofia/FCAPS/build/release/Sofia-PS"]
