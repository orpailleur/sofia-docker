.PHONY: test

OS=$(shell uname -s)

# UID mapping between host and container only matters when running Docker on Linux
ifeq ($(OS),Linux)
MAPUSER=-u $(shell id -u):$(shell id -g)
else
MAPUSER=
endif 

# Shared paths between host and container must be windows path when running Docker under Cygwin
# winpty must be executed as a prefix for a docker run interactive session under Cygwin
ifeq ($(OS),CYGWIN_NT-10.0)
TESTDIR=$(shell cygpath -m ${PWD}/tests)
INTERACTIVE=winpty
else
TESTDIR=${PWD}/tests
INTERACTIVE=
endif 

INAME=qlf-sesi-harbor.inria.fr/orpailleur/sofia
TAG=latest

all:
	@echo "All is in the Makefile"

build_static:
	docker build -t $(INAME):$(TAG) -f Dockerfile.static .

build:
	docker build -t $(INAME):$(TAG) -f Dockerfile .

test:
	docker run --rm $(MAPUSER) -v $(TESTDIR):/tests $(INAME):$(TAG) -CB:/tests/CB.json -data:/tests/Binary_10x10_full/context.json -out
	#docker run --rm $(MAPUSER) -v $(TESTDIR):/tests $(INAME):$(TAG) -CB:/tests/liveinwater/settings.json -data:/tests/liveinwater/context.json -out

push:
	docker push $(INAME):$(TAG)

bash:
	$(INTERACTIVE) docker run -it --rm -v $(TESTDIR):/tests --entrypoint=bash $(INAME):$(TAG)
